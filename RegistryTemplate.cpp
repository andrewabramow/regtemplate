/* ������ ������ �� ��������

��� ����� �������

� ������� �������� ���������� ������� ����� �������, �������� 
������������ ���� �������� �� ������ ������������� ����.

������ ������ ������������ ��������� �������� : ���������� 
�������� � ������, �������� �������� �� �����, ����� ���� 
�������� � ������� � �������, ����� �������� �� �����.

������ ������� ������ �������� �� ��������� � ������ ���������
����� : int, double, std::string.

��� ������������ ��������� ������������� ���� ������ � ������
�� ����������� ������� :

add � �������� ������� � ������;
remove � ������� ������� �� �����;
print � ���������� �� ������ ��� �������� � �� �������;
find � ����� ������� �� �����.

������ � ������������

��� ���������� �� ������ ������������ ����������� ������
std::vector, ������� ����� ������� ���� �����������������
���� ������ � ��������. 
*/

#include <iostream>
#include <vector>
#include <memory>

template <typename T1,typename T2>
class Reg {
private:
    std::vector<std::pair<T1, T2>> reg;
public:
    Reg(std::vector<std::pair<T1, T2>> _reg) : reg(_reg){}
    template <typename T1, typename T2>
    void add(T1 t1, T2 t2) {
        std::pair <T1, T2> new_pair;
        new_pair = std::make_pair(t1, t2);
        reg.push_back(new_pair);
    }
    template <typename T1>
    void remove(T1 t1) {
        for (int i = 0; i < reg.size(); ++i) {
            if (reg[i].first == t1) {
                reg.erase(reg.begin() + i);
            }
        }
    }
    void print() {
        for (int i = 0; i < reg.size(); ++i) {
            std::cout << reg[i].first << " " << reg[i].second << std::endl;
        }
        std::cout << std::endl;
    }
    template <typename T1>
    void find(T1 t1) {
        for (int i = 0; i < reg.size(); ++i) {
            if (reg[i].first == t1) {
                std::cout<< reg[i].second;
            }
        }
        std::cout << std::endl;
    }
};

int main()
{
    // int:
    std::vector<std::pair <int, int>> int_vec;
    int_vec.push_back(std::make_pair(1, 1));
    int_vec.push_back(std::make_pair(2, 2));
    int_vec.push_back(std::make_pair(3, 3));

    std::unique_ptr<Reg <int,int>> reg_int =  std::make_unique<Reg <int, int>>(int_vec);
    std::string temp;
    while (true) {
        std::cout << "What do you want?\n";
        std::cin >> temp;
        if (temp == "add") {
            std::cout << "Type pair:\n";
            int a, b;
            std::cin >> a >> b;
            reg_int->add(a, b);
        }
        else if (temp == "find") {
            std::cout << "Type key:\n";
            int a;
            std::cin >> a;
            reg_int->find(a);
        }
        else if (temp == "remove") {
            std::cout << "Type key:\n";
            int a;
            std::cin >> a;
            reg_int->remove(a);
        }
        else if (temp == "print") {
            reg_int->print();
        }
    }
    

    // string:
    std::vector<std::pair <std::string, std::string>> str_vec;
    str_vec.push_back(std::make_pair("1", "1"));
    str_vec.push_back(std::make_pair("2", "2"));
    str_vec.push_back(std::make_pair("3", "3"));

    std::unique_ptr<Reg <std::string, std::string>> reg_str =
        std::make_unique<Reg <std::string, std::string>>(str_vec);
    reg_str->add("4", "4");
    reg_str->find("4");
    reg_str->remove("4");
    reg_str->print();
}

